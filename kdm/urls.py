
from django.contrib import admin
from django.urls import path, include
from kdm import views

urlpatterns = [

    path('admin/', admin.site.urls), 
    path('', views.home_view, name="home"),
    path('produit/', views.produit_view, name="produit"),
    path('produits/', include('produits.urls')),
    path('users/', include('users.urls')),
]
