#from django.http import HttpResponse
from django.shortcuts import render

def home_view(request):
    return render(request, 'base.html')

def produit_view(request):
    return render(request, 'produit.html')