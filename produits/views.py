from django.shortcuts import render

# Create your views here.
def prod_view(request):
    return render(request, 'prod.html')