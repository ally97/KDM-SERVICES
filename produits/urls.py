from django.urls import path
from produits import views

urlpatterns = [

    path('prod/', views.prod_view, name="prod"),
]